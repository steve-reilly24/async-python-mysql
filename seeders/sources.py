from .base import Seeder

class CallIn(Seeder):
    query_path = './seeders/queries/callin.sql'
    tablename = 'callin'

class CallOut(Seeder):
    query_path = './seeders/queries/callout.sql'
    tablename = 'callout'

class CustProf(Seeder):
    query_path = './seeders/queries/custprof.sql'
    tablename = 'custprof'

class EecNameXref(Seeder):
    query_path = './seeders/queries/eec_name_xref.sql'
    tablename = 'eec_name_xref'

class CfCdrExtras(Seeder):
    query_path = './seeders/queries/cf_cdr_extras.sql'
    tablename = 'cf_cdr_extras'

class Notes(Seeder):
    query_path = './seeders/queries/notes.sql'
    tablename = 'notes'

class CfTags(Seeder):
    query_path = './seeders/queries/cf_tags.sql'
    tablename = 'cf_tags'



# async wrapper functions
async def callin(cursor, bucket, obj_name):
    seeder = CallIn(cursor)
    await seeder.stream_csv_to_s3(bucket, obj_name)

async def callout(cursor, bucket, obj_name):
    seeder = CallOut(cursor)
    await seeder.stream_csv_to_s3(bucket, obj_name)

async def custprof(cursor, bucket, obj_name):
    seeder = CustProf(cursor)
    await seeder.stream_csv_to_s3(bucket, obj_name)

async def eec_name_xref(cursor, bucket, obj_name):
    seeder = EecNameXref(cursor)
    await seeder.stream_csv_to_s3(bucket, obj_name)

async def cf_cdr_extras(cursor, bucket, obj_name):
    seeder = CfCdrExtras(cursor)
    await seeder.stream_csv_to_s3(bucket, obj_name)

async def notes(cursor, bucket, obj_name):
    seeder = Notes(cursor)
    await seeder.stream_csv_to_s3(bucket, obj_name)

async def cf_tags(cursor, bucket, obj_name):
    seeder = CfTags(cursor)
    await seeder.stream_csv_to_s3(bucket, obj_name)
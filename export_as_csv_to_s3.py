from async_db import get_cursor, create_pool
from datetime import datetime, time
from uuid import uuid1
import asyncio
from os import getenv
from dotenv import load_dotenv
from seeders.sources import callin, callout, custprof, eec_name_xref, cf_cdr_extras, notes, cf_tags

def get_rmg_uri():
    uri = getenv('RMG_URI', '')
    assert len(uri), "env var RMG_URI not set"
    return uri

def get_bucket():
    bucket = getenv('S3_TARGET_BUCKET', '')
    assert len(bucket), "env var S3_TARGET_BUCKET not set"
    return bucket

def get_obj_name():
    date = datetime.now()
    year = date.year
    month = date.month
    timestamp = date.strftime("%d_%H:%M:%S")
    
    return f'year={year}/month={month}/{timestamp}'


async def main(loop):
    load_dotenv()
    # validate all required env vars set
    uri = get_rmg_uri()
    bucket = get_bucket()
    obj_name = get_obj_name()
    
    pool = await create_pool(uri=uri,loop=loop)

    await asyncio.gather(
        callin(await get_cursor(await pool.acquire()), bucket, obj_name), 
        callout(await get_cursor(await pool.acquire()), bucket, obj_name),
        custprof(await get_cursor(await pool.acquire()), bucket, obj_name),
        eec_name_xref(await get_cursor(await pool.acquire()), bucket, obj_name),
        cf_cdr_extras(await get_cursor(await pool.acquire()), bucket, obj_name),
        notes(await get_cursor(await pool.acquire()), bucket, obj_name),
        cf_tags(await get_cursor(await pool.acquire()), bucket, obj_name),
    )


loop = asyncio.get_event_loop()
loop.run_until_complete(main(loop))
-- get all records created "yesterday" in rmg.callin for accounts with transcription
SELECT 
  callin.incallid,
  callin.acctnmbr,
  callin.orignmbr,
  callin.tollfree,
  callin.adjusteddatetime,
  callin.call_direction,
  callin.increatetstamp,
  callin.inupdatetstamp
FROM rmg.callin 
JOIN rmg.custprof ON callin.acctnmbr = custprof.acctnmbr
WHERE 
	(callin.increatetstamp >= DATE_SUB(CURDATE(), INTERVAL 1 DAY) -- "yesterday"
	-- (callin.increatetstamp >= DATE_SUB(CURDATE(), INTERVAL 30 DAY) -- "last 30 days"
	  AND callin.increatetstamp < CURDATE()) -- "today"
  -- AND custprof.transcription = '03';

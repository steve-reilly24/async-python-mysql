-- get all records created "yesterday" in rmg.callout for accounts with transcription
SELECT 
  id,
  note,
  callid,
  date_created,
  date_modified
FROM r2.notes
JOIN rmg.callin ON notes.callid = callin.incallid
JOIN rmg.custprof ON callin.acctnmbr = custprof.acctnmbr
WHERE 
	(callin.increatetstamp >= DATE_SUB(CURDATE(), INTERVAL 1 DAY) -- "yesterday"
	-- (callin.increatetstamp >= DATE_SUB(CURDATE(), INTERVAL 30 DAY) -- "last 30 days"
		AND callin.increatetstamp < CURDATE()) -- "today"
  -- AND custprof.transcription = '03';


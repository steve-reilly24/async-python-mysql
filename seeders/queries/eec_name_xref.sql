-- get all records from rmg.custprof with transcription = '03'
SELECT 
    eec_name_xref.id,
    eec_name_xref.acctnmbr,
    eec_name_xref.name,
    eec_name_xref.code,
    eec_name_xref.created,
    eec_name_xref.modified
FROM r2.eec_name_xref
JOIN rmg.custprof ON eec_name_xref.acctnmbr = custprof.acctnmbr
-- WHERE custprof.transcription = '03';


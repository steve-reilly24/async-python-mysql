
import csv

from sys import stdout
from s3 import upload_file
import datetime

class Seeder:
    query_path = ""
    # output_path = ""
    sql = ""
    tablename = ''

    def __init__(self, cursor):   
        self.cursor = cursor
        self._get_seed_query(self.query_path)

    
    async def stream_to_stdout(self):
        assert len(self.sql), "Seeder query not set."

        await self.cursor.execute(self.sql)
        while True:
            row = await self.cursor.fetchone()
            if not row:
                break
            print(type(self).__name__)
    
    
    async def stream_to_csv(self):    
        assert len(self.sql), "Seeder query not set."
        await self.cursor.execute(self.sql)
        output_path = f'./output/{self.tablename}.csv'
        with open(output_path, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow([a_tuple[0] for a_tuple in self.cursor.description])
            while True:
                row = await self.cursor.fetchone()
                if not row:
                    break
                writer.writerow(row)
        

    async def stream_csv_to_s3(self, bucket, obj_name):    
        assert len(self.sql), "Seeder query not set."
        await self.cursor.execute(self.sql)
        
        # breakpoint()
        print("Creating: "+self.tablename)
        output_path = f'./output/{self.tablename}.csv'
        with open(output_path, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow([a_tuple[0] for a_tuple in self.cursor.description])
            while True:
                row = await self.cursor.fetchone()
                if not row:
                    break
                writer.writerow(row)

        if not self.cursor.rownumber:
            print(f'{self.tablename} does not contain any new rows.')
            return

        print("Created: "+self.tablename+"!")
        s3_obj_name = f'{self.tablename}/{obj_name}.csv'
        # breakpoint()
        await upload_file(output_path, bucket=bucket, object_name=s3_obj_name)



    def _get_seed_query(self, file_path):
        with open(file_path) as f:
            for line in f:
                self.sql += line



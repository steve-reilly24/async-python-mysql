# Redshift PoC

This is a place to store migrations, stored procedures, etc. for the Redshift PoC work.

## Requirements
- A copy of the .env file can be found in 1Password
- Access to AWS via a local user profile


## Migrations
Migrations should be run in order. 

### Execution Order

Migration sub-directories are ordered by date. Migrations should be run in date order. Every subdirectory in migrations will have an up.sql and a down.sql

### up.sql

SQL for running migration into DB

### down.sql
SQL for removing changes made by migration 


## Data Extraction
```sh
$ python export_as_csv_to_s3.py
```
### foreach table...
1. Grabs the query from `seeders/queries/`
2. Streams the results of the query into a csv in `output/`
3. Uploads the csv to s3 in the following format `{bucket}/{table_name}/year={year}/month={month}/{day}_{time}.csv`
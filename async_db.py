
import aiomysql
from urllib.parse import urlparse


async def parse_db_uri(db_uri):
    parsed = urlparse(db_uri)
    return {
        "host": parsed.hostname,
        "port": parsed.port,
        "user": parsed.username,
        "password": parsed.password,
        "db": parsed.path[1:],
        # **extra_params,
    }

async def connection(uri, loop):
    conf = await parse_db_uri(uri)
    conn = await aiomysql.connect(host=conf['host'], port=conf['port'],
                                       user=conf['user'], password=conf['password'], 
                                       db=conf['db'], loop=loop)
    return conn

async def create_pool(uri, loop):
    conf = await parse_db_uri(uri)
    pool = await aiomysql.create_pool(host=conf['host'], port=conf['port'],
                                       user=conf['user'], password=conf['password'], 
                                       db=conf['db'], loop=loop)
    return pool

async def get_cursor(conn):
    return await conn.cursor(aiomysql.SSCursor)
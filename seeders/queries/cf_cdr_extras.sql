-- get all records created "yesterday" in rmg.callout for accounts with transcription
SELECT 
	cf_cdr_extras.id,
  cf_cdr_extras.callid,
  cf_cdr_extras.var_name,
  cf_cdr_extras.var_value,
  cf_cdr_extras.created,
  cf_cdr_extras.lastupdate
FROM r2.cf_cdr_extras
JOIN rmg.callin ON cf_cdr_extras.callid = callin.incallid
JOIN rmg.custprof ON callin.acctnmbr = custprof.acctnmbr
WHERE 
	(callin.increatetstamp >= DATE_SUB(CURDATE(), INTERVAL 1 DAY) -- "yesterday"
	-- (callin.increatetstamp >= DATE_SUB(CURDATE(), INTERVAL 30 DAY) -- "last 30 days"
		AND callin.increatetstamp < CURDATE()) -- "today"
  -- AND custprof.transcription = '03';

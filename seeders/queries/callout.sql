-- get all records created "yesterday" in rmg.callout for accounts with transcription
SELECT 
	callout.outcallid,
  callout.incallid,
  callout.employeecode,
  callout.connectduration,
  callout.pots,
  callout.outcreatetstamp,
  callout.outupdatetstamp
FROM rmg.callout
JOIN rmg.callin ON callout.incallid = callin.incallid
JOIN rmg.custprof ON callin.acctnmbr = custprof.acctnmbr
WHERE 
	(callin.increatetstamp >= DATE_SUB(CURDATE(), INTERVAL 1 DAY) -- "yesterday"
	-- (callin.increatetstamp >= DATE_SUB(CURDATE(), INTERVAL 30 DAY) -- "last 30 days"
		AND callin.increatetstamp < CURDATE()) -- "today"
  -- AND custprof.transcription = '03';
